FROM python:3.9-buster     
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY /app /app
EXPOSE 5000
ENTRYPOINT [ "python" ]
CMD ["app.py" ]